var onTop=1;

$(document).ready(function(){
	start(50);
	
});

//add circle
$(".addCirc").click(function(){
	start(1);
}); 

// changeColor
$(".changeColor").click(function(){
	var arr = document.querySelectorAll("#circle");
	for(var i = 0; i < arr.length; i++) {
		arr[i].style.backgroundColor = getRandomColor();
		arr[i].style.borderColor = getRandomColor();
	}
}); 

//remove all circle
$(".removeCirc").click(function(){
	 $("section").empty();
	 start(50);
});

//delete on double click
$("#canvas").on('dblclick', function(event) {
	if (event.target.id != "canvas")
		$(event.target).remove();
		
});

// bring to front
$(".myContainer").on('click', function(event){
		$(event.target).css("z-index", onTop);
		onTop = 1+parseInt($(event.target).css("z-index"));
});

var animation;
var running = false;
//start animate
$(".startAnimation").click(function(){
	clearInterval(animation);
	running = !running;
	if (!running)
		return;
	animation = stopIt =setInterval(function(){
			var arr = document.querySelectorAll("#circle");
			for(var i = 0; i < arr.length; i++) {
				var x =getRandomX();
				var y =getRandomY();
				$(arr[i]).animate({left:x,top:y},3000,'linear');
				arr[i].style.backgroundColor = getRandomColor();
				if (x >= 645 || x <= 3 || y >= 445 || y <= 3)
					arr[i].style.borderColor = getRandomColor();
			}
		},3000);
}); 

//puts x num of circles
function start(x){
	for (i=0; i<x; i++){
		var newCirc = $("<div />");
		newCirc.attr("id","circle");
		addCirclesCss(newCirc);
		$(".myContainer").append(newCirc);
	}
}

//gets css for the new circle
function addCirclesCss(x){
	return $(x).css({"background-color": getRandomColor(), 
					"border-color": getRandomColor(), 
					left : getRandomX(),
					top : getRandomY() });
	
}
function getRandomColor(){
		var letters= "0123456789abcdef";
		var result= "#";
		
		for(var i=0; i<6; i++){	
			result += letters.charAt(parseInt(Math.random() * letters.length));
		}
		return result;
}

function getRandomX(){
	return(Math.random() * ($(".myContainer").width() - 50)); 
}
function getRandomY(){
	return(Math.random() * ($(".myContainer").height() - 50));
}
